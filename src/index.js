import { Navigation } from 'react-native-navigation';
import { goToLogin } from './lib/navigation/navigation';

import Login from './views/Auth/Login';
import Offers from './views/Offers';
import Profile from './views/Profile';
import Rating from './views/Rating';
import Rentals from './views/Rentals';
import OfferDetail from './views/OfferDetail';
import OfferCreate from './views/OfferCreate';

Navigation.registerComponent('screen.Login', () => Login);
Navigation.registerComponent('screen.Offers', () => Offers);
Navigation.registerComponent('screen.Rentals', () => Rentals);
Navigation.registerComponent('screen.Profile', () => Profile);
Navigation.registerComponent('screen.Rating', () => Rating);
Navigation.registerComponent('screen.OfferDetail', () => OfferDetail);
Navigation.registerComponent('screen.OfferCreate', () => OfferCreate);

Navigation.events().registerAppLaunchedListener(() => {
  Navigation.setDefaultOptions({
    topBar: {
      visible: true,
      drawBehind: false,
      hideOnScroll: false,
      backButton: {
        icon: (require('./assets/img/icons/backArrow.png')),
      },
      title: {
        fontSize: 15,
        color: '#fff',
      },
      background: {
        color: '#00430E',
      },
    },
    layout: {
      backgroundColor: '#fff',
      orientation: ['portrait'],
    },
    bottomTabs: {
      titleDisplayMode: 'alwaysShow',
      animate: false,
    },
  });

  goToLogin();
});
