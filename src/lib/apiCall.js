export const apiCall = async ({
  endpoint,
  method,
  headers,
  body,
}) => {
  return fetch(endpoint, {
    method: !method ? 'GET' : method,
    headers: {
      // Accept: 'application/json',
      // 'Content-Type': 'application/json',
      ...headers,
    },
    body: body ? body : undefined,
   //  body: method && (method !== 'GET' || method !== 'HEAD') ?
   //    JSON.stringify({
   //      ...body,
   //    }) :
   //    undefined,
  });
};

