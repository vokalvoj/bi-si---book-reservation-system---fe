import { Navigation } from 'react-native-navigation';

export const goToLogin = () => Navigation.setRoot({
  root: {
    stack: {
      id: 'Auth',
      children: [
        {
          component: {
            name: 'screen.Login',
          },
        },
      ],
    },
  },
});


export const goHome = () => Navigation.setRoot({
  root: {
    bottomTabs: {
      id: 'BottomTabsId',
      children: [
        {
          stack: {
            children: [
              {
                component: {
                  name: 'screen.Offers',
                  options: {
                    bottomTab: {
                      text: 'OFFERS',
                      fontSize: 10,
                      selectedFontSize: 10,
                     //  textColor: '#9EC73D',
                     //  selectedTextColor: '#fff',
                     //  selectedIconColor: 'white',
                      icon: require('../../assets/img/icons/project.png'),
                    },
                  },
                },
              },
            ],
          },
        },
        {
          stack: {
            children: [
              {
                component: {
                  name: 'screen.Rentals',
                  options: {
                    bottomTab: {
                      text: 'RENTALS',
                      fontSize: 10,
                      selectedFontSize: 10,
                      icon: require('../../assets/img/icons/news.png'),
                    },
                  },
                },
              },
            ],
          },
        },
        {
          stack: {
            children: [
              {
                component: {
                  name: 'screen.Profile',
                  options: {
                    bottomTab: {
                      text: 'PROFILE',
                      fontSize: 10,
                      selectedFontSize: 10,
                      icon: require('../../assets/img/icons/user.png'),
                    },
                  },
                },
              },
            ],
          },
        },
      ],
    },
  },
});
