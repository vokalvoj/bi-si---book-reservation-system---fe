import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

class Rentals extends Component {
  static options() {
    return {
      topBar: {
        visible: false,
        height: 0,
      },
    };
  }

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.mainWrapper}>
        <Text style={styles.header}>Rentals</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainWrapper: {
    paddingVertical: 15,
    paddingHorizontal: 10,
  },
  header: {
    marginTop: 30,
    fontWeight: 'bold',
    fontSize: 24,
    alignSelf: 'center',
    color: '#228B22',
  },
});

export default Rentals;

