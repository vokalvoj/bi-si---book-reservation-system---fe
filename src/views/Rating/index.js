import React, { Component } from 'react';
import { Navigation } from 'react-native-navigation';
import { FlatList, View, Text, StyleSheet, Alert } from 'react-native';

import { getProfileRating } from '../../api/profile';

class Rating extends Component {
  static options() {
    return {
      topBar: {
        title: {
          text: 'My rating',
        },
      },
    };
  }

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);

    this.state = {
      isLoading: true,
      reviews: [],
    };
  }

  componentDidMount() {
    this.getRating();
  }

  async getRating() {
    try {
      const reviews = await getProfileRating();

      this.setState({
        reviews: reviews.reviews,
      });
    } catch (error) {
      Alert.alert('Error', 'Cannot get user reviews from API.');
    } finally {
      this.setState({ isLoading: false });
    }
  }

  render() {
    const { reviews, isLoading } = this.state;

    let averageRating = 0;
    reviews.forEach(item => {averageRating += item.rating;});
    averageRating /= reviews.length;

    return (
      <View>
        {averageRating ?
          <Text style={styles.overallRating}>
            Overall rating: {averageRating}
          </Text> : null
        }
        <FlatList
          data={reviews}
          refreshing={isLoading}
          onRefresh={() => {}}
          keyExtractor={(item, index) => `${index}`}
          showsVerticalScrollIndicator={false}
          renderItem={({ item }) =>
            <View style={styles.reviewWrapper}>
              <Text style={styles.name}>{item.name}</Text>
              <Text style={styles.rating}>Rating: {item.rating}/5</Text>
              {item.text &&
                <Text style={styles.ratingText}>{item.text}</Text>
              }
            </View>
          }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainWrapper: {
    paddingBottom: 15,
    paddingHorizontal: 20,
    alignItems: 'center',
  },
  overallRating: {
    color: '#228B22',
    marginTop: 12,
    fontSize: 22,
    lineHeight: 25,
    alignSelf: 'center',
    fontWeight: 'bold',
  },
  reviewWrapper: {
    padding: 10,
    marginHorizontal: 10,
    backgroundColor: '#DDD',
    borderRadius: 10,
    borderWidth: 1,
    marginVertical: 10,
    alignItems: 'center',
  },
  name: {
    fontSize: 17,
    lineHeight: 20,
    fontWeight: '700',
    color: '#228B22',
  },
  rating: {
    fontSize: 13,
    lineHeight: 16,
    marginTop: 2,
    marginBottom: 5,
  },
  ratingText: {
    fontSize: 14,
    lineHeight: 15,
  },
});

export default Rating;

