import React, { Component } from 'react';
import { ScrollView, View, Text, StyleSheet, TextInput, Picker } from 'react-native';

import Button from '../../components/layout/Button';

class OfferCreate extends Component {
  static options() {
    return {
      topBar: {
        title: {
          text: 'New offer',
        },
      },
    };
  }

  state = {
    language: 'Java',
  };

  render() {
    return (
      <ScrollView>
        <View style={styles.mainWrapper}>
          <Text style={styles.label}>Title:</Text>
          <TextInput
            style={styles.input}
          />
          <Text style={styles.label}>Author:</Text>
          <TextInput
            style={styles.input}
          />
          <Text style={styles.label}>Price (Kč):</Text>
          <TextInput
            style={styles.input}
          />
          <Text style={styles.label}>Offer type:</Text>
          <View style={styles.input}>
            <Picker
              selectedValue={this.state.language}
              style={styles.input}
              onValueChange={(itemValue, itemIndex) =>
                this.setState({language: itemValue})
              }>
              <Picker.Item label="For sale" value="forSale" />
              <Picker.Item label="Auction" value="auction" />
            </Picker>
          </View>
          <Button
            title={'Create offer'}
          />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  mainWrapper: {
    flex: 1,
    justifyContent: 'flex-start',
    paddingHorizontal: 10,
  },
  label: {
    fontSize: 18,
    marginTop: 15,
    marginBottom: 8,
  },
  input: {
    fontSize: 17,
    paddingHorizontal: 10,
    borderColor: 'gray',
    borderRadius: 10,
    height: 40,
    borderWidth: 1,
  },
});

export default OfferCreate;

