import React, { Component, Fragment } from 'react';
import { Navigation } from 'react-native-navigation';
import { View, Text, StyleSheet, Alert, Image, ActivityIndicator, AsyncStorage } from 'react-native';

import { getProfile } from '../../api/profile';
import { goToLogin } from '../../lib/navigation/navigation';
import AvatarPlaceholder from '../../assets/img/icons/avatarPlaceholder.png';
import Logout from '../../assets/img/icons/share.png';
import Button from '../../components/layout/Button';

class Profile extends Component {
  static options() {
    return {
      topBar: {
        title: {
          text: 'My profile',
        },
        rightButtons: [
          {
            id: 'Logout',
            icon: Logout,
          },
        ],
      },
    };
  }

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);

    this.state = {
      isLoading: true,
      profile: null,
    };
  }

  componentDidMount() {
    this.getProfile();
  }

  async getProfile() {
    this.setState({ isLoading: true });

    const userId = AsyncStorage.getItem('userId');
    const userToken = AsyncStorage.getItem('token');

    try {
      const profile = await getProfile(userId, userToken);

      if (typeof profile === 'number') {
        Alert.alert('Error', 'Cannot get user');
        return;
      }

      this.setState({
        profile: profile,
      });
    } catch (error) {
      Alert.alert('Error', 'Cannot get profile from API.');
    } finally {
      this.setState({ isLoading: false });
    }
  }

  navigationButtonPressed({ buttonId }) {
    if (buttonId === 'Logout') {
      // TODO: Handle logout
      goToLogin();
    }
  }

  handleMyRatingRedirect = () => {
    Navigation.push(this.props.componentId, {
      component: {
        name: 'screen.Rating',
      },
    });
  };

  render() {
    const { profile, isLoading } = this.state;

    if (isLoading || !profile) {
      return (
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <ActivityIndicator size="large" />
        </View>
      );
    }

    return (
      <View>
        <View style={styles.topBar} />
        <View style={styles.mainWrapper}>
          {isLoading ?
            <ActivityIndicator size="large" /> :
            <Fragment>
              <View style={styles.avatar}>
                <Image
                  source={AvatarPlaceholder}
                />
              </View>

              <Text style={styles.fullName}>
                {profile.name}
              </Text>
              <Text style={styles.email}>{profile.email}</Text>

              <Button
                title={'My rating'}
                style={styles.button}
                textStyle={styles.buttonText}
                onPress={this.handleMyRatingRedirect}
              />
            </Fragment>
          }
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainWrapper: {
    paddingBottom: 15,
    paddingHorizontal: 10,
    alignItems: 'center',
  },
  topBar: {
    height: 200,
    backgroundColor: '#228B22',
  },
  avatar: {
    marginTop: -65,
    width: 140,
    height: 140,
    borderRadius: 70,
    borderWidth: 5,
    borderColor: 'white',
    backgroundColor: 'white',
  },
  fullName: {
    marginTop: 20,
    fontSize: 25,
    fontWeight: 'bold',
  },
  email: {
    fontSize: 15,
    marginVertical: 4,
  },
  phone: {
    fontSize: 15,
    marginVertical: 4,
  },
  button: {
    width: 250,
    borderRadius: 30,
    marginTop: 30,
  },
  buttonText: {
    fontWeight: 'normal',
  },
});

export default Profile;

