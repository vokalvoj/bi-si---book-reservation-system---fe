import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, Alert, ActivityIndicator, AsyncStorage } from 'react-native';

import { goHome } from '../../lib/navigation/navigation';
import { login } from '../../api/profile';
import Button from '../../components/layout/Button';

class Login extends Component {
  static options() {
    return {
      topBar: {
        visible: false,
        height: 0,
      },
    };
  }

  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      username: '',
      password: '',
    };
  }

  handleLogin = async () => {
    const { username, password } = this.state;

    this.setState({ isLoading: true });

    try {
      const userDetails = await login(username, password);

      if (typeof usedDetail === 'number' || userDetails === 401) {
        Alert.alert('Error', 'Invalid login credentials');
        return;
      }

      if (userDetails.token) {
        console.log('USER_DETAILS', userDetails);
        await AsyncStorage.setItem('token', `${userDetails.token}`);
        await AsyncStorage.setItem('userId', `${userDetails.personId}`);
        goHome();
      }
    } catch (error) {
      console.log(error);
    } finally {
      this.setState({ isLoading: false });
    }
  };

  render() {
    const { username, password, isLoading } = this.state;

    if (isLoading) {
      return (
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <ActivityIndicator size="large" />
        </View>
      );
    }
    return (
      <View style={styles.mainWrapper}>
        <Text style={styles.header}>Login</Text>
        <Text style={styles.label}>Username:</Text>
        <TextInput
          style={styles.input}
          value={username}
          onChangeText={(username) => this.setState({ username })}
        />
        <Text style={styles.label}>Password:</Text>
        <TextInput
          style={styles.input}
          value={password}
          onChangeText={(password) => this.setState({ password })}
          secureTextEntry
        />
        <Button
          title={'Login'}
          onPress={this.handleLogin}
        />
        <Text
          style={styles.withoutLogin}
          onPress={goHome}
        >
          Continue without login >>
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainWrapper: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  header: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#228B22',
    alignSelf: 'center',
  },
  label: {
    fontSize: 18,
    marginTop: 15,
    marginBottom: 8,
  },
  input: {
    fontSize: 17,
    paddingHorizontal: 10,
    borderColor: 'gray',
    borderRadius: 10,
    height: 40,
    borderWidth: 1,
  },
  withoutLogin: {
    alignSelf: 'center',
    paddingVertical: 5,
    fontSize: 14,
  },
});

export default Login;

