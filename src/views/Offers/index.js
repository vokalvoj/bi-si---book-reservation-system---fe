import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, TouchableOpacity, Alert, AsyncStorage } from 'react-native';
import { Navigation } from 'react-native-navigation';

import { getOffers } from '../../api/offers';
import Offer from '../../components/Offers/Offer';

class Offers extends Component {
  static options() {
    return {
      topBar: {
        visible: false,
        height: 0,
      },
    };
  }

  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      books: [],
    };
  }

  componentDidMount() {
    this.getOffers();
  }

  async getOffers() {
    this.setState({ isLoading: true });

    const userToken = AsyncStorage.getItem('token');

    try {
      const offers = await getOffers(userToken);

      if (typeof profile === 'number') {
        Alert.alert('Error', 'Cannot get offers');
        return;
      }

      this.setState({
        books: [...offers],
      });
    } catch (error) {
      Alert.alert('Error', 'Cannot get offers from API.');
    } finally {
      this.setState({ isLoading: false });
    }
  }

  handleRedirect = (item) => {
    Navigation.push(this.props.componentId, {
      component: {
        name: 'screen.OfferDetail',
        passProps: { item },
      },
    });
  };

  handleCreateOffer = () => {
    Navigation.push(this.props.componentId, {
      component: {
        name: 'screen.OfferCreate',
      },
    });
  };

  renderCreateOffer() {
    return (
      <TouchableOpacity
        onPress={this.handleCreateOffer}
      >
        <View style={styles.buttonWrapper}>
          <Text style={styles.buttonPlus}>+</Text>
          <Text style={styles.buttonText}>Create offer</Text>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    const { books, isLoading } = this.state;

    return (
      <View style={styles.mainWrapper}>
        <FlatList
          data={books}
          refreshing={isLoading}
          onRefresh={() => {}}
          keyExtractor={(item, index) => `${index}`}
          showsVerticalScrollIndicator={false}
          ListHeaderComponent={() =>
            <View>
              <Text style={styles.header}>Offers</Text>
              {this.renderCreateOffer()}
            </View>
          }
          renderItem={({ item }) =>
            <TouchableOpacity
              onPress={() => this.handleRedirect(item)}
            >
              <Offer
                offerType={item.type}
                price={item.price}
                book={item.books[0]}
                author={item.author}
              />
            </TouchableOpacity>
          }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainWrapper: {
    flex: 1,
    paddingVertical: 15,
    paddingHorizontal: 10,
  },
  header: {
    marginTop: 30,
    fontWeight: 'bold',
    fontSize: 24,
    alignSelf: 'center',
    color: '#228B22',
  },
  buttonWrapper: {
    marginTop: 15,
    height: 45,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: '#000',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonPlus: {
    fontSize: 20,
    fontWeight: 'bold',
    marginRight: 10,
  },
  buttonText: {
    fontSize: 20,
  },
});

export default Offers;

