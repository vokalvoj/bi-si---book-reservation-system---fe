import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';

import AvatarPlaceholder from '../../assets/img/icons/avatarPlaceholder.png';
import Button from '../../components/layout/Button';

class OfferDetail extends Component {
  static options() {
    return {
      topBar: {
        title: {
          text: 'Offer detail',
        },
      },
    };
  }

  render() {
    const { book, author, price, offerType } = this.props.item;

    return (
      <View style={styles.mainWrapper}>
        <Text style={styles.bookName}>{book.title}</Text>
        <View style={styles.tableWrapper}>
          <View style={styles.leftSide}>
            <Text style={styles.leftText}>Author:</Text>
            <Text style={styles.leftText}>Released:</Text>
            <Text style={styles.leftText}>Offer type:</Text>
          </View>
          <View style={styles.rightSide}>
            <Text style={styles.rightText}>{book.author}</Text>
            <Text style={styles.rightText}>{book.released}</Text>
            <Text style={styles.rightText}>{offerType}</Text>
          </View>
        </View>

        <Text style={styles.priceText}>
          Price:
          <Text style={styles.price}>
            {` ${price} Kč`}
          </Text>
        </Text>

        <Text style={styles.seller}>Seller:</Text>
        <View style={styles.sellerWrapper}>
          <View style={styles.avatarWrapper}>
            <Image
              source={AvatarPlaceholder}
              style={styles.avatar}
            />
          </View>
          <View style={styles.authorDetails}>
            <Text style={styles.authorName}>{author.firstName} {author.lastName}</Text>
            <Text style={styles.authorRating}>Rating: {author.rating}/5 </Text>
          </View>
        </View>
        <Button
          title={offerType.toLowerCase() === 'auction' ? 'Bid' : 'Request an offer'}
          style={styles.button}
          textStyle={styles.buttonText}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainWrapper: {
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
  bookName: {
    marginTop: 10,
    fontWeight: 'bold',
    fontSize: 26,
    marginBottom: 8,
    color: '#228B22',
  },
  tableWrapper: {
    flexDirection: 'row',
    // borderBottomWidth: 1,
    // borderColor: '#000',
  },
  leftSide: {
    paddingRight: 20,
  },
  leftText: {
    fontSize: 14,
  },
  rightText: {
    fontSize: 14,
    fontWeight: '700',
  },
  priceText: {
    marginTop: 20,
    fontSize: 23,
  },
  price: {
    fontWeight: 'bold',
  },
  seller: {
    marginTop: 20,
    marginBottom: 10,
    fontSize: 23,
    color: '#228B22',
  },
  sellerWrapper: {
    flexDirection: 'row',
    backgroundColor: '#DDD',
    padding: 10,
    borderRadius: 20,
    borderWidth: 1,
    width: '100%',
  },
  avatarWrapper: {
    height: 70,
    width: 70,
    overflow: 'hidden',
    borderRadius: 35,
    backgroundColor: 'white',
  },
  avatar: {
    height: 70,
    width: 70,
  },
  authorDetails: {
    marginLeft: 18,
    justifyContent: 'center',
    paddingBottom: 10,
  },
  authorName: {
    fontSize: 17,
    fontWeight: 'bold',
    color: '#228B22',
  },
  authorRating: {
    fontSize: 14,
  },
  button: {
    alignSelf: 'center',
    height: 45,
    width: '100%',
  },
  buttonText: {
    fontSize: 17,
  },
});

export default OfferDetail;

