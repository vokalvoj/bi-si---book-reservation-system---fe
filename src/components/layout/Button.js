import React from 'react';
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

const Button = (props) => {
  return (
    <TouchableOpacity
      onPress={props.onPress}
    >
      <View
        style={{
          ...styles.wrapper,
          ...props.style,
        }}
      >
        <Text
          style={{
            ...styles.text,
            ...props.textStyle,
          }}
        >
          {props.title}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

Button.propTypes = {
  title: PropTypes.string.isRequired,
  onPress: PropTypes.func,
  style: PropTypes.object,
  textStyle: PropTypes.object,
};

const styles = StyleSheet.create({
  wrapper: {
    height: 50,
    marginVertical: 15,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    backgroundColor: '#228B22',
  },
  text: {
    fontSize: 22,
    fontWeight: 'bold',
    color: '#fff',
  },
});

export default Button;
