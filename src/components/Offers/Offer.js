import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import PropTypes from 'prop-types';

import AvatarPlaceholder from '../../assets/img/icons/avatarPlaceholder.png';

const Offer = (props) => {
  const { author, book, offerType, price } = props;

  return (
    <View style={styles.mainWrapper}>
      <View style={styles.leftSide}>
        <View style={styles.authorWrapper}>
          <Image
            source={AvatarPlaceholder}
            style={styles.authorAvatar}
          />
        </View>
        <Text style={styles.authorName}>{author.name}</Text>
      </View>
      <View style={styles.rightSide}>
        <Text style={styles.bookName}>{book.name}</Text>
        <Text style={styles.bookAuthor}>{book.authorName}</Text>
        <Text style={styles.offerType}>Type: {offerType}</Text>
        <Text style={styles.offerPrice}>Price: {price} Kč</Text>
      </View>
    </View>
  );
};

Offer.propTypes = {
  book: PropTypes.object.isRequired,
  author: PropTypes.object.isRequired,
  price: PropTypes.number,
};

const styles = StyleSheet.create({
  mainWrapper: {
    width: '100%',
    backgroundColor: '#DDD',
    flexDirection: 'row',
    borderRadius: 10,
    borderWidth: 1,
    marginVertical: 10,
  },
  leftSide: {
    paddingVertical: 10,
    width: 150,
    alignItems: 'center',
    justifyContent: 'center',
  },
  rightSide: {
    paddingHorizontal: 10,
    paddingBottom: 5,
    flex: 1,
    justifyContent: 'center',
  },
  authorWrapper: {
    borderRadius: 30,
    backgroundColor: 'white',
    overflow: 'hidden',
    width: 60,
    height: 60,
  },
  authorAvatar: {
    width: 60,
    height: 60,
  },
  authorName: {
    marginTop: 3,
    fontSize: 15,
    fontWeight: '700',
  },
  authorRating: {
    fontSize: 12,
  },
  bookName: {
    fontSize: 17,
    lineHeight: 20,
    fontWeight: '700',
    color: '#228B22',
  },
  bookAuthor: {
    fontSize: 14,
    marginLeft: 5,
  },
  offerType: {
    fontSize: 13,
    marginLeft: 5,
  },
  offerPrice: {
    fontSize: 14,
    marginTop: 10,
    marginLeft: 5,
    fontWeight: '700',
  },
});

export default Offer;

