import { endpoint } from '../lib/config';
import { apiCall } from '../lib/apiCall';

export const getOffers = async (token) => {
  const options = {
    endpoint: `${endpoint}/offer?token=${token}`,
  };

  try {
    const response = await apiCall(options);

    if (response.ok) {
      return response.json();
    }

    return response.status;
  } catch (error) {
    console.log(error);
  }
};

