import { endpoint, endpointMock } from '../lib/config';
import { apiCall } from '../lib/apiCall';

export const login = async (email, password) => {
  const options = {
    method: 'POST',
    endpoint: `${endpoint}/user/login?email=${email}&password=${password}`,
  };

  try {
    const response = await apiCall(options);

    if (response.ok) {
      return response.json();
    }

    return response.status;
  } catch (error) {
    console.log(error);
  }
};

export const getProfile = async (userId, token) => {
  const options = {
    endpoint: `${endpoint}/user/${userId}?token=${token}`,
  };

  try {
    const response = await apiCall(options);

    if (response.ok) {
      return response.json();
    }

    return response.status;
  } catch (error) {
    console.log(error);
  }
};

export const getProfileRating = async () => {
  const options = {
    endpoint: `${endpointMock}/profile/rating`,
  };

  try {
    const response = await apiCall(options);

    return await response.json();
  } catch (error) {
    console.log(error);
  }
};

